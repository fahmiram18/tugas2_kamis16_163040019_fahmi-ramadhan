<script type="text/javascript">
	$(document).ready(function() {
		$('#tbl_produk').DataTable({
		    "language": {
	            "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
			    "sProcessing":   "Sedang memproses...",
			    "sLengthMenu":   "Tampilkan _MENU_ entri",
			    "sZeroRecords":  "Tidak ditemukan data yang sesuai",
			    "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
			    "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
			    "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
			    "sInfoPostFix":  "",
			    "sSearch":       "Cari:",
			    "sUrl":          "",
			    "oPaginate": {
			        "sFirst":    "Pertama",
			        "sPrevious": "Sebelumnya",
			        "sNext":     "Selanjutnya",
			        "sLast":     "Terakhir"
			    }
	        },
			"processing": true,
			"serverSide": true,
			"iDisplayLength": 5,
    		"aLengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "Semua"]],
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('produk/get_data_produk')?>",
				"type": "POST"
			},
			columnDefs: [{
				"targets": [ 0, 1 ],
				"orderable": false,
			}],           
		});       
	});
	function handleClickUbah(id) {
		$.ajax({
            type: "POST",
            url: "<?php echo base_url('produk/get_dataById/') ?>"+id,
            data: "id=" + id,
            dataType: "json",
            success: function(data) {
            	console.log(data);
            	// var a = JSON.parse(data);
                $("#id_ubah").val(data.id_produk);
                $("#id_produk").val(data.id_produk);
                $("#gambar_lama").val(data.gambar_produk);
                $("#nama_produk").val(data.nama_produk);
                $("#tahun_produk").val(data.tahun_produk);
                $("#stok").val(data.stok);
                $("#harga").val(data.harga);
                $("#kategori_produk").val(data.kategori_produk);
            }
        });
    };  
</script>
</body>
</html>