<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Administrator</p>
      </div>
    </div>
    <!-- search form -->
    <!-- <form action="#" method="post" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Cari...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
            </button>
          </span>
      </div>
    </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGASI</li>
        <li>
          <a href="<?php echo base_url(); ?>dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview menu-open">
          <a href="#">
            <i class="fa fa-table"></i> <span>Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" style="display: block;">
            <li>
              <a href="<?php echo base_url(); ?>produk/tambah">
                <i class="fa fa-circle-o"></i> <span>Tambah Produk</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>produk">
                  <i class="fa fa-circle-o"></i> <span>Kelola Produk</span>
              </a>
            </li>
            <li>
              <a href="<?php echo base_url(); ?>pesanan">
                  <i class="fa fa-circle-o"></i> <span>Kelola Pesanan</span>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>