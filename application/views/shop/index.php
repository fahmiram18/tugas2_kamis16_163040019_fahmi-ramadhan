<div class="container">
	<div class="row mt-5">
		<div class="col">
			<div class="row">
				<?php foreach ($products as $product): ?>
					<div class="col mt-4 mb-2">
						<div class="card border border-secondary" style="width: 10rem; height: 23rem;">
							<img style="width: 100%" class="card-img-top p-2" src="<?php echo site_url('assets/img/produk/').$product->gambar_produk ?>" alt="Card image cap">
							<div class="card-body" style="font-size: .8rem;">
								<h6 class="card-title"><?php echo $product->nama_produk; ?></h6>
								<p class="card-text"><?php echo 'Rp. '.number_format($product->harga,0,',','.').',-'; ?></p>
								<p class="card-text">Stok : <b><?php echo $product->stok != 0?$product->stok:'Tidak Tersedia';?></p></b>
								<a href="#" class="btn btn-info" data-toggle="modal" data-target="#detailModal" onclick="handleClickDetail(<?php echo $product->id_produk ?>)"><i class="fas fa-info"></i></a>
								<button type="button" name="button" class="btn btn-success add_cart" id="add_cart"
									data-harga='<?php echo $product->harga; ?>'
									data-nama='<?php echo $product->nama_produk; ?>'
									data-id='<?php echo $product->id_produk; ?>'
								><i class="fas fa-plus"></i></button>
							</div>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<ul class="list-group">
		  <li class="list-group-item" id="harga">Cras justo odio</li>
		  <li class="list-group-item" id="stok">Dapibus ac facilisis in</li>
		  <li class="list-group-item" id="tahun">Morbi leo risus</li>
		  <li class="list-group-item" id="kategori">Porta ac consectetur ac</li>
		</ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" name="button" class="btn btn-success add_cart" id="add_cart"
			data-harga='<?php echo $product->harga; ?>'
			data-nama='<?php echo $product->nama_produk; ?>'
			data-id='<?php echo $product->id_produk; ?>'
		><i class="fas fa-plus"></i> Tambah</button>
      </div>
    </div>
  </div>
</div>