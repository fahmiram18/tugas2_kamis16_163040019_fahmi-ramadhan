<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function validation($username)
	{
		$this->db->where('username', $username);
		$this->db->get('tbl_admin');
		return $this->db->affected_rows();
	}

	public function getDataAdmin($username)
	{
		$this->db->where('username', $username);
		return $this->db->get('tbl_admin')->row();
	}
}

/* End of file Admin_model.php */
/* Location: ./application/models/Admin_model.php */