<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function count_data($table)
	{
		return $this->db->count_all_results($table);
	}

}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */