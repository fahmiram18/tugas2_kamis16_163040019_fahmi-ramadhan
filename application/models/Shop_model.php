<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends CI_Model {

	public function getAllProduk()
	{
		return $this->db->get('tbl_produk')->result();	
	}

	public function getDataById($id)
	{
		$this->db->where('id_produk', $id);
		return $this->db->get('tbl_produk')->row();
	}

}

/* End of file Shop_model.php */
/* Location: ./application/models/Shop_model.php */