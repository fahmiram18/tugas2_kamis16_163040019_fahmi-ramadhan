<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
	}

	public function _status()
	{
		
	}

	public function index()
	{
		if (!$this->session->userdata('username')) {
			redirect('admin/login');
		}
	}

	public function login()
	{
		if ($this->session->userdata('username')) {
			redirect('dashboard');
		}
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$data['judul'] = 'AdminLogin';
			$this->load->view('admin/login_form', $data);
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
					
			if ($this->Admin_model->validation($username) > 0) {
				$row = $this->Admin_model->getDataAdmin($username);
				if (password_verify($password, $row->password)) {
					$this->session->set_userdata('username', $username);
					$this->session->set_userdata('hash', hash('sha256', $row->id_admin, false));
				}
				if (hash('sha256', $row->id_admin) == $this->session->userdata('hash')) {
					redirect('dashboard');
				}
			} else {
				$data['judul'] = 'AdminLogin';
				$this->load->view('admin/login_form', $data);
				$this->session->set_flashdata('status', 
	                '<div class="alert alert-danger alert-dismissible" style="position:fixed; right:0; z-index:100;">
	                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
	                    Username atau Password salah!
	                </div>'
	            );
			}
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('hash');
		redirect('admin/login');
	}

	public function _rules()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" style="position:fixed; right:0; z-index:100;">
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	        <h4><i class="icon fa fa-ban"></i> Alert!</h4>', '</div>'
	    );
    }
}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */